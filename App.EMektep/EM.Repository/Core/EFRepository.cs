﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace EM.Repository {
    internal class EFRepository<TContext, TEntity> : IGenericRepository<TEntity>
        where TContext : DbContext
        where TEntity : class {

        public EFRepository() {
            EntitySet = Context.Set<TEntity>();
        }

        private Lazy<TContext> lazyCtx { get; } =
            new Lazy<TContext>(() => Activator.CreateInstance<TContext>());
        private TContext Context {
            get {
                return lazyCtx.Value;
            }
        }
        private DbSet<TEntity> _EntitySet;
        private DbSet<TEntity> EntitySet {
            get {
                return _EntitySet ?? (_EntitySet = Context.Set<TEntity>());
            }
            set {
                _EntitySet = value;
            }
        }

        public async Task<IDMLResponse<TEntity>> Delete<TKey>(TKey key) {
            var existing = await EntitySet.FindAsync(key);
            if (existing != null) {
                try {
                    Context.Entry(existing).State = EntityState.Deleted;
                    await Context.SaveChangesAsync();
                    return new DMLResponse<TEntity>() {
                        Success = true
                    };
                } catch (Exception ex) {
                    return new DMLResponse<TEntity>() {
                        Fault = ex,
                        Success = false
                    };
                }
            } else {
                return new DMLResponse<TEntity>() {
                    Fault = new Exception($"Entity ID: {key} not found"),
                    Success = false
                };
            }
        }

        public async Task<IDMLResponse<TEntity>> Insert(TEntity entity) {
            try {
                EntitySet.Add(entity);
                await Context.SaveChangesAsync();
                return new DMLResponse<TEntity>(entity) {
                    Success = true
                };
            } catch (Exception ex) {
                return new DMLResponse<TEntity>() {
                    Fault = ex,
                    Success = false
                } as IDMLResponse<TEntity>;
            }
        }
        public async Task<IDMLResponse<TEntity>> Update<TKey>(TKey key, TEntity entity, params Func<TEntity, object>[] relationSelectors) {
            try {
                entity = EntitySet.Attach(entity);
                foreach (var selector in relationSelectors) {
                    Context.Entry(selector(entity)).State = EntityState.Modified;
                }
                var rowc = await Context.SaveChangesAsync();
                return new DMLResponse<TEntity>(entity) {
                    Fault = rowc == 0 ? new Exception("Güncelleme Başarısız") : null,
                    Success = rowc > 0
                };
            } catch (Exception ex) {
                return new DMLResponse<TEntity>() {
                    Fault = ex,
                    Success = false
                };
            }
        }
        public async Task<IDMLResponse<TEntity>> SelectAll() {
            return new DMLResponse<TEntity>(await EntitySet.ToListAsync());
        }

        public async Task<IDMLResponse<TEntity>> SelectBy(params Expression<Func<TEntity, bool>>[] selectors) {
            var list = new List<TEntity>();
            foreach (var selector in selectors) {
                list.AddRange(await EntitySet.Where(selector).ToListAsync());
            }
            return new DMLResponse<TEntity>(list);
        }

        public async Task<IDMLResponse<TEntity>> SelectSingle(Expression<Func<TEntity, bool>> selector) {
            return new DMLResponse<TEntity>(await EntitySet.Where(selector).FirstOrDefaultAsync());
        }

        public async Task<IDMLResponse<TEntity>> SelectSingleByID<TKey>(TKey key) {
            return new DMLResponse<TEntity>(await EntitySet.FindAsync(key));
        }

    }
}
