﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM.Repository {

    public interface IDMLResponse<TEntity> 
        where TEntity : class {

        string Message { get; }
        bool Success { get; }
        Exception Fault { get; }
        ICollection<TEntity> CollectionResponse { get; }
        TEntity SingleResponse { get; }

    }
}
