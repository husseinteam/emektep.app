﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace EM.Repository {
    public interface IGenericRepository<TEntity>
            where TEntity : class {

        Task<IDMLResponse<TEntity>> Insert(TEntity entity);
        Task<IDMLResponse<TEntity>> Update<TKey>(TKey key, TEntity entity, params Func<TEntity, object>[] relationSelectors);
        Task<IDMLResponse<TEntity>> Delete<TKey>(TKey key);
        Task<IDMLResponse<TEntity>> SelectAll();
        Task<IDMLResponse<TEntity>> SelectBy(params Expression<Func<TEntity, bool>>[] selectors);
        Task<IDMLResponse<TEntity>> SelectSingle(Expression<Func<TEntity, bool>> selector);
        Task<IDMLResponse<TEntity>> SelectSingleByID<TKey>(TKey key);

    }
}
