﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM.Repository {

    internal class DMLResponse<TEntity> : IDMLResponse<TEntity>
        where TEntity : class {

        public Exception Fault { get; internal set; }
        public bool Success { get; internal set; }
        public string Message { get; internal set; }

        public DMLResponse() {

        }
        public DMLResponse(ICollection<TEntity> collection) {
            this.CollectionResponse = collection;
        }

        public DMLResponse(TEntity item) {
            this.SingleResponse = item;
        }

        public ICollection<TEntity> CollectionResponse { get; private set; }
        public TEntity SingleResponse { get; private set; }

    }
}
