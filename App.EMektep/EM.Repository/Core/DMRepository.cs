﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using DM.Domain.Objects;
using DM.Domain.Query;

namespace EM.Repository {
    internal class DMRepository<TEntity> : DMSyncRepository<TEntity>, IGenericRepository<TEntity>
        where TEntity : DOBase<TEntity> {

        public DMRepository(Func<Exception, IDMLResponse<TEntity>> fallback) 
            : base(fallback) {

        }

        public DMRepository() : base() {

        }

        public async Task<IDMLResponse<TEntity>> Delete<TKey>(TKey key) {
            return await Task.Run(() => DeleteSync(key));
        }

        public async Task<IDMLResponse<TEntity>> Insert(TEntity entity) {
            return await Task.Run(() => InsertSync(entity));
        }
        public async Task<IDMLResponse<TEntity>> Update<TKey>(TKey key, TEntity entity, params Func<TEntity, object>[] relationSelectors) {
            return await Task.Run(() => UpdateSync(key, entity, relationSelectors));
        }
        public async Task<IDMLResponse<TEntity>> SelectAll() {
            return await Task.Run(() => SelectAllSync());
        }

        public async Task<IDMLResponse<TEntity>> SelectBy(params Expression<Func<TEntity, bool>>[] selectors) {
            return await Task.Run(() => SelectBySync(selectors));
        }

        public async Task<IDMLResponse<TEntity>> SelectSingle(Expression<Func<TEntity, bool>> selector) {
            return await Task.Run(() => SelectSingleSync(selector));
        }

        public async Task<IDMLResponse<TEntity>> SelectSingleByID<TKey>(TKey key) {
            return await Task.Run(() => SelectSingleByIDSync(key));
        }

    }
}
