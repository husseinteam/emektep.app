﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DM.Domain.Objects;

namespace EM.Repository.Relayer {
    public static class SRepository {

        public static IGenericRepository<TEntity> GenerateEFRepository<TContext, TEntity>()
            where TEntity : class
            where TContext : DbContext {
            return new EFRepository<TContext, TEntity>();
        }

        public static IGenericRepository<TEntity> GenerateDMRepository<TEntity>()
            where TEntity : DOBase<TEntity> {
            return new DMRepository<TEntity>();
        }

    }
}
