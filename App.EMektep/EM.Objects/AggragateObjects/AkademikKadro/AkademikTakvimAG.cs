﻿using System;
using DM.Domain.Aggregate;
using EM.Entities.DomainObjects.Mufredat;
using EM.Entities.DomainObjects.Uyelik;
using EM.Extension.Core;
using EM.Objects.DomainObjects.AkademikKadro;
using EM.Objects.DomainObjects.Kurum;
using EM.Objects.DomainObjects.Mufredat;

namespace EM.Entities.AggragateObjects.AkademikKadro {

    public class AkademikTakvimAG : AGBase<AkademikTakvimAG> {

        public DateTime Baslangic { get; set; }

        public DateTime Bitis { get; set; }

        public string Etkinlik { get; set; }

        public string FakulteAdi { get; set; }

        public string UniversiteAdi { get; set; }

        public EAkademikDonem AkademikDonem { get; set; }

        protected override void Map(IAGViewBuilder<AkademikTakvimAG> builder) {

            builder
                .MapsTo(x => x.SchemaName("AGG").ViewName("AkademikTakvim"))
                .Select<AkademikTakvim>(li =>
                    li.Add(x => x.Baslangic, x => x.Baslangic)
                    .Add(x => x.Bitis, x => x.Bitis)
                    .Add(x => x.Aciklama, x => x.Etkinlik)
                    .Add(x => x.AkademikDonem, x => x.AkademikDonem)
                )
                .OuterJoin<Fakulte>(li => li.Add(x => x.Adi, x => x.FakulteAdi))
                    .On<AkademikTakvim>((fl, at) => fl.ID == at.FakulteID)
                .OuterJoin<Universite>(li => li.Add(x => x.Adi, x => x.UniversiteAdi))
                    .On<Fakulte>((uv, fl) => fl.UniversiteID == uv.ID);

        }

    }

}
