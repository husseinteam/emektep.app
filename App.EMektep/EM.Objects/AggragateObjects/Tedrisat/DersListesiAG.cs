﻿using DM.Domain.Aggregate;
using EM.Entities.DomainObjects.Uyelik;
using EM.Objects.DomainObjects.AkademikKadro;
using EM.Objects.DomainObjects.Kurum;
using EM.Objects.DomainObjects.Mufredat;

namespace EM.Entities.AggragateObjects.Tedrisat {

    public class DersListesiAG : AGBase<DersListesiAG> {

        public int DersID { get; set; }

        public int DonemID { get; set; }

        public int UyeID { get; set; }

        public string DersKodu { get; set; }

        public string DersAdi { get; set; }

        public int TeorikSaat { get; set; }

        public int UygulamaSaat { get; set; }

        public int Kredi { get; set; }

        public string OgretimUyesi { get; set; }

        protected override void Map(IAGViewBuilder<DersListesiAG> builder) {

            builder
                .MapsTo(x => x.SchemaName("AGG").ViewName("Dersler"))
                .Select<DersYuku>()
                .InnerJoin<Program>().On<DersYuku>((prg, dy) => prg.ID == dy.ProgramID)
                .InnerJoin<OgretimUyesi>().On<DersYuku>((ogu, dy) => ogu.ID == dy.OgretimUyesiID)
                .InnerJoin<Uye>(li => 
                    li.Add(x => x.FullName, x => x.OgretimUyesi)
                    .Add(x => x.ID, x => x.UyeID)
                ).On<OgretimUyesi>((uy, ogu) => uy.ID == ogu.UyeID)
                .InnerJoin<Mufredat>().On<Program>((mf, prg) => mf.ProgramID == prg.ID)
                .InnerJoin<Donem>(li => li.Add(d => d.ID, x => x.DonemID))
                    .On<Mufredat>((dn, mf) => dn.MufredatID == mf.ID)
                .InnerJoin<Ders>(li => 
                    li.Add(d => d.DersKodu, x => x.DersKodu)
                    .Add(d => d.DersAdi, x => x.DersAdi)
                    .Add(d => d.TeorikSaat, x => x.TeorikSaat)
                    .Add(d => d.UygulamaSaat, x => x.UygulamaSaat)
                    .Add(d => d.Kredi, x => x.Kredi)
                    .Add(x => x.ID, x => x.DersID))
                .On<Donem>((drs, dn) => drs.DonemID == dn.ID);

        }

    }

}
