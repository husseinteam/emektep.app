﻿using DM.Domain.Aggregate;
using EM.Entities.DomainObjects.AkademikKadro;
using EM.Entities.DomainObjects.Uyelik;
using EM.Objects.DomainObjects.AkademikKadro;
using EM.Objects.DomainObjects.Kurum;
using EM.Objects.DomainObjects.Mufredat;

namespace EM.Entities.AggragateObjects.Tedrisat {

    public class OgrenciListesiAG : AGBase<OgrenciListesiAG> {

        public int DersID { get; set; }

        public int UyeID { get; set; }

        public int OgretimUyesiUyeID { get; set; }

        public string OgrenciNo { get; set; }

        public string OgrenciAdiSoyadi { get; set; }
        
        public int OgrenciKayitYili { get; set; }

        public EKayitlanmaTipi OgrenciKayitTipi { get; set; }

        protected override void Map(IAGViewBuilder<OgrenciListesiAG> builder) {

            builder
                .MapsTo(x => x.SchemaName("AGG").ViewName("Ogrenciler"))
                .Select<DersYuku>()
                .InnerJoin<Program>().On<DersYuku>((prg, dy) => prg.ID == dy.ProgramID)
                .InnerJoin<OgretimUyesi>(li => li
                        .Add(x => x.ID, x => x.OgretimUyesiUyeID)
                    ).On<DersYuku>((ogu, dy) => ogu.ID == dy.OgretimUyesiID)
                .InnerJoin<DersProgrami>().On<DersYuku>((dp, dy) => dp.DersYukuID == dy.ID)
                .InnerJoin<Ogrenci>(li => li
                        .Add(x => x.OgrenciNo, x => x.OgrenciNo)
                        .Add(x => x.KayitlanmaTipi, x => x.OgrenciKayitTipi)
                        .Add(x => x.KayitYili, x => x.OgrenciKayitYili)
                    ).On<DersProgrami>((og, dp) => og.ID == dp.OgrenciID)
                .InnerJoin<Uye>(li => li
                        .Add(x => x.FullName, x => x.OgrenciAdiSoyadi)
                        .Add(x => x.ID, x => x.UyeID)
                    ).On<Ogrenci>((uy, og) => uy.ID == og.UyeID)
                .InnerJoin<Mufredat>().On<Program>((mf, prg) => mf.ProgramID == prg.ID)
                .InnerJoin<Donem>().On<Mufredat>((dn, mf) => dn.MufredatID == mf.ID)
                .InnerJoin<Ders>(li =>
                    li.Add(d => d.ID, x => x.DersID))
                    .On<Donem>((drs, dn) => drs.DonemID == dn.ID);

        }

    }

}
