﻿using System;
using DM.Domain.Aggregate;
using EM.Entities.DomainObjects.Karsilama;
using EM.Entities.DomainObjects.Mufredat;
using EM.Entities.DomainObjects.Uyelik;
using EM.Extension.Core;
using EM.Objects.DomainObjects.AkademikKadro;
using EM.Objects.DomainObjects.Kurum;
using EM.Objects.DomainObjects.Mufredat;

namespace EM.Entities.AggragateObjects.Karsilama {

    public class BulletinAG : AGBase<BulletinAG> {

        public string Baslik { get; set; }

        public DateTime YayimTarihi { get; set; }

        public string OnYazi { get; set; }

        public string Etiketler { get; set; }

        public string BaslikResmi { get; set; }

        public EBulletinType Tip { get; set; }

        protected override void Map(IAGViewBuilder<BulletinAG> builder) {

            builder
                .MapsTo(x => x.SchemaName("AGG").ViewName("Duyurular"))
                .Select<Bulletin>(li =>
                    li.Add(x => x.Baslik, x => x.Baslik)
                    .Add(x => x.YayimTarihi, x => x.YayimTarihi)
                    .Add(x => x.OnYazi, x => x.OnYazi)
                    .Add(x => x.Etiketler, x => x.Etiketler)
                    .Add(x => x.BaslikResmi, x => x.BaslikResmi)
                    .Add(x => x.Tip, x => x.Tip)
                );

        }

    }

}
