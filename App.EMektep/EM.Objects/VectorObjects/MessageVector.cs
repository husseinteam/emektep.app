﻿namespace EM.Entities.VectorObjects {

    public class MessageVector : TokenVector {

        public bool done { get; set; }

        public object data { get; set; }

        public string[] messages { get; set; }

    }

}
