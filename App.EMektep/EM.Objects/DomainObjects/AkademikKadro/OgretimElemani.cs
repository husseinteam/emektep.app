﻿using DM.Domain.Objects;
using EM.Entities.DomainObjects.Uyelik;
using EM.Objects.DomainObjects.Core;

namespace EM.Objects.DomainObjects.AkademikKadro {

    public class OgretimElemani : EMBase<OgretimElemani> {

        public string Ad { get; set; }

        public string Soyadi { get; set; }

        public int UyeID { get; set; }

        protected override void EMMap(IDOTableBuilder<OgretimElemani> builder) {
            
            builder.MapsTo(x => { x.SchemaName("AK").TableName("OgretimElemanlari"); });
            builder.For(d => d.Ad).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(64);
            builder.For(d => d.Soyadi).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(64);

            builder.ForeignKey(x => x.UyeID).References<Uye>(u => u.ID);
            builder.UniqueKey(x => x.UyeID);

        }

    }
}
