﻿using DM.Domain.Objects;
using EM.Entities.DomainObjects.Uyelik;
using EM.Objects.DomainObjects.Core;

namespace EM.Objects.DomainObjects.AkademikKadro {

    public class OgretimUyesi : EMBase<OgretimUyesi> {

        public string Unvani { get; set; }

        public int UyeID { get; set; }

        protected override void EMMap(IDOTableBuilder<OgretimUyesi> builder) {
            
            builder.MapsTo(x => { x.SchemaName("AK").TableName("OgretimUyeleri"); });
            builder.For(d => d.Unvani).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(64);

            builder.ForeignKey(x => x.UyeID).References<Uye>(u => u.ID);
            builder.UniqueKey(x => x.UyeID);

        }

    }
}
