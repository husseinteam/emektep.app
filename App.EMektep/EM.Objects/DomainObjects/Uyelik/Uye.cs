﻿using System;
using DM.Domain.Objects;
using EM.Objects.DomainObjects.Core;

namespace EM.Entities.DomainObjects.Uyelik {

    public class Uye : EMBase<Uye> {

        public string FullName { get; set; }
        
        public string Contact { get; set; }

        public string TCKN { get; set; }

        public string Password { get; set; }

        public string AccessToken { get; set; }

        public string ActivationToken { get; set; }

        public string RecoveryToken { get; set; }

        public DateTime ActivationTokenExpiresOn { get; set; }

        public DateTime AccessTokenExpiresOn { get; set; }

        public DateTime RecoveryTokenExpiresOn { get; set; }

        protected override void EMMap(IDOTableBuilder<Uye> builder) {

            builder.MapsTo(x => { x.SchemaName("MS").TableName("Uyeler"); });

            builder.For(d => d.Contact).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(255);
            builder.For(d => d.Password).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(64);
            builder.For(d => d.FullName).IsTypeOf(EDataType.String).HasMaxLength(128);
            builder.For(d => d.TCKN).IsTypeOf(EDataType.String).HasMaxLength(11);

            builder.For(d => d.ActivationToken).IsTypeOf(EDataType.String).HasMaxLength(128);
            builder.For(d => d.AccessToken).IsTypeOf(EDataType.String).HasMaxLength(128);
            builder.For(d => d.RecoveryToken).IsTypeOf(EDataType.String).HasMaxLength(128);

            builder.For(d => d.ActivationTokenExpiresOn).IsTypeOf(EDataType.DateTime);
            builder.For(d => d.AccessTokenExpiresOn).IsTypeOf(EDataType.DateTime);
            builder.For(d => d.RecoveryTokenExpiresOn).IsTypeOf(EDataType.DateTime);

            builder.UniqueKey(d => d.TCKN);

            builder.UniqueKey(x => x.Contact);
        }

    }

}
