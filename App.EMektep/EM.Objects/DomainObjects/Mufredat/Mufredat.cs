﻿using DM.Domain.Objects;
using EM.Objects.DomainObjects.Core;
using EM.Objects.DomainObjects.Kurum;

namespace EM.Objects.DomainObjects.Mufredat {

    public class Mufredat : EMBase<Mufredat> {

        public string Adi { get; set; }

        public int YururlukYili { get; set; }

        public int ProgramID { get; set; }

        protected override void EMMap(IDOTableBuilder<Mufredat> builder) {
            
            builder.MapsTo(x => { x.SchemaName("MF").TableName("Mufredatlar"); });
            builder.For(d => d.Adi).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(128);
            builder.For(d => d.YururlukYili).IsTypeOf(EDataType.Int).IsRequired();

            builder.ForeignKey(x => x.ProgramID).References<Program>(x => x.ID);

        }

    }
}
