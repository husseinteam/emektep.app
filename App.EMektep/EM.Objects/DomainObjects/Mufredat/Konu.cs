﻿using DM.Domain.Objects;
using EM.Objects.DomainObjects.Core;

namespace EM.Objects.DomainObjects.Mufredat {

    public class Konu : EMBase<Konu> {

        public string Adi { get; set; }
    
        public int DersID { get; set; }

        protected override void EMMap(IDOTableBuilder<Konu> builder) {
            
            builder.MapsTo(x => { x.SchemaName("MF").TableName("Konular"); });
            builder.For(d => d.Adi).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(128);

            builder.ForeignKey(x => x.DersID).References<Ders>(x => x.ID);
        }

    }
}
