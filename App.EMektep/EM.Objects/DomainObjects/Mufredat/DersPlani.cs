﻿using System;
using DM.Domain.Objects;
using EM.Objects.DomainObjects.Core;

namespace EM.Objects.DomainObjects.Mufredat {

    public class DersPlani : EMBase<DersPlani> {

        public DateTime DersTarihi { get; set; }

        public int DersID { get; set; }

        protected override void EMMap(IDOTableBuilder<DersPlani> builder) {
            
            builder.MapsTo(x => { x.SchemaName("MF").TableName("DersPlani"); });
            builder.For(d => d.DersTarihi).IsTypeOf(EDataType.DateTime).IsRequired();

            builder.ForeignKey(tc => tc.DersID).References<Ders>(s => s.ID);

        }

    }
}
