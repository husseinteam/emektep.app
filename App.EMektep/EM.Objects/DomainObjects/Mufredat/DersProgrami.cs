﻿using DM.Domain.Objects;
using EM.Objects.DomainObjects.AkademikKadro;
using EM.Objects.DomainObjects.Core;

namespace EM.Objects.DomainObjects.Mufredat {

    public class DersProgrami : EMBase<DersProgrami> {

        public string Baslik { get; set; }

        public int DersYukuID { get; set; }

        public int OgrenciID { get; set; }

        protected override void EMMap(IDOTableBuilder<DersProgrami> builder) {
            
            builder.MapsTo(x => { x.SchemaName("MF").TableName("DersProgrami"); });
            builder.For(d => d.Baslik).IsTypeOf(EDataType.String).IsRequired();

            builder.ForeignKey(tc => tc.DersYukuID).References<DersYuku>(s => s.ID);
            builder.ForeignKey(tc => tc.OgrenciID).References<Ogrenci>(s => s.ID);

        }

    }
}
