﻿using DM.Domain.Objects;
using EM.Objects.DomainObjects.Core;

namespace EM.Objects.DomainObjects.Mufredat {

    public class Donem : EMBase<Donem> {

        public string DonemAdi { get; set; }

        public int MufredatID { get; set; }

        protected override void EMMap(IDOTableBuilder<Donem> builder) {
            
            builder.MapsTo(x => { x.SchemaName("MF").TableName("Donemler"); });
            builder.For(d => d.DonemAdi).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(128);

            builder.ForeignKey(x => x.MufredatID).References<Mufredat>(x => x.ID);

        }

    }
}
