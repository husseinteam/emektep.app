﻿
using System.ComponentModel;

namespace EM.Entities.DomainObjects.Mufredat {

    public enum EAkademikDonem {

        [Description("yariyil1")]
        BirinciYariyil = 1,
        [Description("yariyil2")]
        IkinciYariyil,
        [Description("yazogretimi")]
        YazDonemi,
        [Description("Donem1")]
        Donem1,
        [Description("Donem2")]
        Donem2,
        [Description("Donem3")]
        Donem3,
        [Description("Donem4")]
        Donem4,
        [Description("Donem5")]
        Donem5,
        [Description("Donem6")]
        Donem6

    }

}