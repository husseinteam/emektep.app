﻿using System;
using DM.Domain.Objects;
using EM.Entities.DomainObjects.Mufredat;
using EM.Objects.DomainObjects.AkademikKadro;
using EM.Objects.DomainObjects.Core;
using EM.Objects.DomainObjects.Kurum;

namespace EM.Objects.DomainObjects.Mufredat {

    public class AkademikTakvim : EMBase<AkademikTakvim> {

        public DateTime Baslangic { get; set; }

        public DateTime Bitis { get; set; }

        public string Aciklama { get; set; }

        public int? FakulteID { get; set; }

        public EAkademikDonem AkademikDonem { get; set; }

        protected override void EMMap(IDOTableBuilder<AkademikTakvim> builder) {

            builder.MapsTo(x => { x.SchemaName("MF").TableName("AkademikTakvim"); });
            builder.For(d => d.Baslangic).IsTypeOf(EDataType.Date).IsRequired();
            builder.For(d => d.Bitis).IsTypeOf(EDataType.Date);
            builder.For(d => d.Aciklama).IsTypeOf(EDataType.String).HasMaxLength(400).IsRequired();
            builder.For(d => d.AkademikDonem).IsTypeOf(EDataType.Enum).IsRequired();

            builder.ForeignKey(x => x.FakulteID).References<Fakulte>(x => x.ID);

        }

    }
}
