﻿using DM.Domain.Objects;
using EM.Objects.DomainObjects.AkademikKadro;
using EM.Objects.DomainObjects.Core;
using EM.Objects.DomainObjects.Kurum;

namespace EM.Objects.DomainObjects.Mufredat {

    public class DersYuku : EMBase<DersYuku> {

        public string Baslik { get; set; }

        public int OgretimUyesiID { get; set; }

        public int ProgramID { get; set; }

        protected override void EMMap(IDOTableBuilder<DersYuku> builder) {
            
            builder.MapsTo(x => { x.SchemaName("MF").TableName("DersYuku"); });
            builder.For(d => d.Baslik).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(128);

            builder.ForeignKey(tc => tc.OgretimUyesiID).References<OgretimUyesi>(s => s.ID);
            builder.ForeignKey(tc => tc.ProgramID).References<Program>(s => s.ID);

        }

    }
}
