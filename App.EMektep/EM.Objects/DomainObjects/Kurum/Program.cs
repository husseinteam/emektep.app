﻿using DM.Domain.Objects;
using EM.Objects.DomainObjects.Core;

namespace EM.Objects.DomainObjects.Kurum {

    public class Program : EMBase<Program> {

        public string Adi { get; set; }

        public int MYOID { get; set; }

        protected override void EMMap(IDOTableBuilder<Program> builder) {
            
            builder.MapsTo(x => { x.SchemaName("KR").TableName("Programlar"); });
            builder.For(d => d.Adi).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(128);

            builder.ForeignKey(x => x.MYOID).References<MYO>(x => x.ID);

        }

    }
}
