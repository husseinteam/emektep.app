﻿using DM.Domain.Objects;
using EM.Objects.DomainObjects.Core;

namespace EM.Objects.DomainObjects.Kurum {

    public class Fakulte : EMBase<Fakulte> {

        public string Adi { get; set; }
        
        public int UniversiteID { get; set; }

        protected override void EMMap(IDOTableBuilder<Fakulte> builder) {
            
            builder.MapsTo(x => { x.SchemaName("KR").TableName("Fakulteler"); });
            builder.For(d => d.Adi).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(128);

            builder.ForeignKey(x => x.UniversiteID).References<Universite>(x => x.ID);

        }

    }
}
