﻿using System;
using DM.Domain.Objects;
using EM.Entities.DomainObjects.Karsilama;
using EM.Entities.DomainObjects.Mufredat;
using EM.Objects.DomainObjects.AkademikKadro;
using EM.Objects.DomainObjects.Core;
using EM.Objects.DomainObjects.Kurum;

namespace EM.Objects.DomainObjects.Mufredat {

    public class Bulletin : EMBase<Bulletin> {

        public string Baslik { get; set; }

        public DateTime YayimTarihi { get; set; }

        public string OnYazi { get; set; }

        public string Etiketler { get; set; }

        public string BaslikResmi { get; set; }

        public EBulletinType Tip { get; set; }

        protected override void EMMap(IDOTableBuilder<Bulletin> builder) {

            builder.MapsTo(x => { x.SchemaName("PL").TableName("Duyurular"); });
            builder.For(d => d.Baslik).IsTypeOf(EDataType.String).HasMaxLength(128).IsRequired();
            builder.For(d => d.YayimTarihi).IsTypeOf(EDataType.Date);
            builder.For(d => d.OnYazi).IsTypeOf(EDataType.String).HasMaxLength(400).IsRequired();
            builder.For(d => d.Etiketler).IsTypeOf(EDataType.String).HasMaxLength(128);
            builder.For(d => d.BaslikResmi).IsTypeOf(EDataType.String).HasMaxLength(255).IsRequired();
            builder.For(d => d.Tip).IsTypeOf(EDataType.Enum).IsRequired();

        }

    }
}
