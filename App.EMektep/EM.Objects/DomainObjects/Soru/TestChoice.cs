﻿using DM.Domain.Objects;
using EM.Objects.DomainObjects.Core;

namespace EM.Objects.DomainObjects.Soru {

    public class TestChoice : EMBase<TestChoice> {

        public string Text { get; set; }

        public bool Correct { get; set; }

        public int Value { get; set; }

        public int QuestionID { get; set; }

        protected override void EMMap(IDOTableBuilder<TestChoice> builder) {
            
            builder.MapsTo(x => { x.SchemaName("SV").TableName("TestChoices"); });
            builder.For(d => d.Text).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(800);
            builder.For(d => d.Correct).IsTypeOf(EDataType.Bool).IsRequired();
            builder.For(d => d.Value).IsTypeOf(EDataType.Int).IsRequired();

            builder.ForeignKey(tc => tc.QuestionID).References<Question>(s => s.ID);

        }

    }
}
