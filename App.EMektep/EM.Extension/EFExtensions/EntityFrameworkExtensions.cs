﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EM.Extension.EFExtensions {
    public static class EntityFrameworkExtensions {

        public static PrimitivePropertyConfiguration HasUniqueIndex(this PrimitivePropertyConfiguration cfg) {
            return cfg.HasColumnAnnotation(IndexAnnotation.AnnotationName, 
                new IndexAnnotation(new IndexAttribute() { IsUnique = true }));
        }

    }
}
