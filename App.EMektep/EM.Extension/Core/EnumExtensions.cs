﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace EM.Extension.Core {
    public static class EnumExtensions {

        public static string GetDescription<T>(this T value) {
            if (typeof(T).IsEnum) {
                var instance = Activator.CreateInstance<T>();
                return typeof(T).GetFields().Single(fi => fi.GetValue(value).Equals(value)).GetCustomAttribute<DescriptionAttribute>().Description;
            } else {
                throw new ArgumentException("T is not an enum");
            }
        }

        public static TEnum GenerateEnum<TEnum>(this string description) {

            return Enum.GetValues(typeof(TEnum)).OfType<TEnum>().Single(eval => eval.GetDescription() == description);

        }

    }
}
