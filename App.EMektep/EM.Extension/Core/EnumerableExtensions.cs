﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EM.Extension.Core {
    public static class EnumerableExtensions {

        public static IList<T> Append<T>(this IList<T> self, T item) {

            self.Add(item);
            return self;

        }

        public static void Each<T>(this IEnumerable<T> self, Action<T> cursor) {

            foreach (var item in self) {
                cursor(item);
            }

        }

        public static void Meanwhile<T>(this T self, Func<bool> selector, Action<T, int> cursor) {

            int i = 0;
            while (selector()) {
                cursor(self, i++);
            }

        }

    }
}
