namespace EM.App.Configurations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EnabledCascadeOnUserDeleted : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("Api.AspNetUsers", "UserProfileID", "Api.UserProfiles");
            AddForeignKey("Api.AspNetUsers", "UserProfileID", "Api.UserProfiles", "ID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("Api.AspNetUsers", "UserProfileID", "Api.UserProfiles");
            AddForeignKey("Api.AspNetUsers", "UserProfileID", "Api.UserProfiles", "ID");
        }
    }
}
