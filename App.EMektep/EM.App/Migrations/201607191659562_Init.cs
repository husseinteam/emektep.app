namespace EM.App.Configurations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Api.Lectures",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Code = c.String(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "Api.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "Api.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("Api.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("Api.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "Api.UserProfiles",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Avatar = c.Binary(),
                        Position = c.String(nullable: false),
                        PhoneAlternate = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "Api.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                        ParentID = c.String(maxLength: 128),
                        UserProfileID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Api.AspNetUsers", t => t.ParentID)
                .ForeignKey("Api.UserProfiles", t => t.UserProfileID)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex")
                .Index(t => t.ParentID)
                .Index(t => t.UserProfileID, unique: true, name: "UQ_UserProfileID");
            
            CreateTable(
                "Api.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Api.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "Api.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("Api.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("Api.AspNetUsers", "UserProfileID", "Api.UserProfiles");
            DropForeignKey("Api.AspNetUserRoles", "UserId", "Api.AspNetUsers");
            DropForeignKey("Api.AspNetUsers", "ParentID", "Api.AspNetUsers");
            DropForeignKey("Api.AspNetUserLogins", "UserId", "Api.AspNetUsers");
            DropForeignKey("Api.AspNetUserClaims", "UserId", "Api.AspNetUsers");
            DropForeignKey("Api.AspNetUserRoles", "RoleId", "Api.AspNetRoles");
            DropIndex("Api.AspNetUserLogins", new[] { "UserId" });
            DropIndex("Api.AspNetUserClaims", new[] { "UserId" });
            DropIndex("Api.AspNetUsers", "UQ_UserProfileID");
            DropIndex("Api.AspNetUsers", new[] { "ParentID" });
            DropIndex("Api.AspNetUsers", "UserNameIndex");
            DropIndex("Api.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("Api.AspNetUserRoles", new[] { "UserId" });
            DropIndex("Api.AspNetRoles", "RoleNameIndex");
            DropTable("Api.AspNetUserLogins");
            DropTable("Api.AspNetUserClaims");
            DropTable("Api.AspNetUsers");
            DropTable("Api.UserProfiles");
            DropTable("Api.AspNetUserRoles");
            DropTable("Api.AspNetRoles");
            DropTable("Api.Lectures");
        }
    }
}
