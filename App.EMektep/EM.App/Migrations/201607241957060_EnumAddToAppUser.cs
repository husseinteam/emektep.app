namespace EM.App.Configurations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EnumAddToAppUser : DbMigration
    {
        public override void Up()
        {
            AddColumn("Api.AspNetUsers", "UserType", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("Api.AspNetUsers", "UserType");
        }
    }
}
