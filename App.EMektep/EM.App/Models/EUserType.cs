﻿using System.ComponentModel;

namespace EM.App.Models {
    public enum EUserType {

        [Description("admin")]
        Admin = 0,
        [Description("ogretmen")]
        Ogretmen, 
        [Description("veli")]
        Veli,
        [Description("ogrenci")]
        Ogrenci

    }
}