﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.Validation;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity.EntityFramework;
using EM.App.Configurations;
using EM.Extension.EFExtensions;

namespace EM.App.Models {
    [DbConfigurationType(typeof(InitializerConfiguration))]
    public class ApiDbContext : IdentityDbContext<ApplicationUser> {
#if DEBUG
        public ApiDbContext() : base("LocalSqlConnection", throwIfV1Schema: false) {
        }
#else
        public ApiDbContext() : base("LLSqlConnection", throwIfV1Schema: false) {
        }
#endif


        public static ApiDbContext Create() {
            return new ApiDbContext();
        }
        public DbSet<UserProfile> UserProfiles { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder) {
            modelBuilder.HasDefaultSchema("Api");

            modelBuilder.Entity<ApplicationUser>().HasRequired(au => au.UserProfile).WithOptional(up => up.ApplicationUser).Map(x => x.MapKey("UserProfileID")
                .HasColumnAnnotation(
                "UserProfileID", IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("UQ_UserProfileID", 1) { IsUnique = true }))).WillCascadeOnDelete(true);
            modelBuilder.Entity<ApplicationUser>().HasOptional(au => au.Parent).WithMany(au => au.Children).Map(x => x.MapKey("ParentID")).WillCascadeOnDelete(false);
            base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges() {
            try {
                return base.SaveChanges();
            } catch (DbEntityValidationException e) {
                foreach (var eve in e.EntityValidationErrors) {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors) {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
        }
        
    }
}