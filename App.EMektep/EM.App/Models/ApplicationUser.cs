﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Newtonsoft.Json;

namespace EM.App.Models {
    public class ApplicationUser : IdentityUser {
        public ApplicationUser() {
        }
        public ApplicationUser(string username) : base(username) {

        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public EUserType UserType{ get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager) {
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            return userIdentity;
        }
        
        public virtual UserProfile UserProfile { get; set; }
        public virtual ApplicationUser Parent { get; set; }
        public virtual ICollection<ApplicationUser> Children { get; set; }
    }
    public class ApplicationRole : IdentityRole {
        public ApplicationRole() {
        }

        public ApplicationRole(string roleName) : base(roleName) {
        }
    }
    public class UserProfile {
        [Key]
        public int ID { get; set; }

        public byte[] Avatar { get; set; }
        [Required]
        public string Position { get; set; }
        public string PhoneAlternate { get; set; }

        [JsonIgnore]
        public virtual ApplicationUser ApplicationUser { get; set; }

    }
    
}
