﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using EM.App.ViewModels.Account;
using RestSharp;

namespace EM.App.Services {
    internal static class AuthExtensions {
        internal static TokenResponseViewModel RequestToken(this ApiController api, LoginViewModel vm) {
            var client = new RestClient();
            var baseUri = new Uri(
                api.Request.RequestUri.Scheme + "://" + api.Request.RequestUri.Authority);
            client.BaseUrl = baseUri;
            var request = new RestRequest("token", Method.POST);
            request.AddHeader("Accept", "application/json");
            request.RequestFormat = DataFormat.Json;
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.AddParameter("grant_type", "password");
            request.AddParameter("username", vm.Email);
            request.AddParameter("password", vm.Password);
            var response = client.Execute<TokenResponseViewModel>(request);
            return response.Data;
        }

    }
}