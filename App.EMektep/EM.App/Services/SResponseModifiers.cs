﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EM.App.Models;

namespace EM.App.Services {

    public static class SResponseModifiers {

        public static object UserModifier(ApplicationUser appuser) {
            return new {
                id = appuser.Id,
                Email = appuser.Email,
                FirstName = appuser.FirstName,
                LastName = appuser.LastName,
                PhoneNumber = appuser.PhoneNumber,
                UserProfile = new {
                    ID = appuser.UserProfile.ID,
                    Position = appuser.UserProfile.Position,
                    PhoneAlternate = appuser.UserProfile.PhoneAlternate
                }
            };
        }

    }

}