﻿using System.Threading.Tasks;

namespace EM.App.Services {
    public interface IEmailSender {
        Task SendEmailAsync(string email, string subject, string message);
    }
}
