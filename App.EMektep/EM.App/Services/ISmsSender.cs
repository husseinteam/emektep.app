﻿using System.Threading.Tasks;

namespace EM.App.Services {
    public interface ISmsSender {
        Task SendSmsAsync(string number, string message);
    }
}
