﻿using Microsoft.Owin;
using Owin;
using System;
using System.Web.Http;
using Microsoft.Owin.Security.OAuth;
using EM.App.OAuth.Providers;
using EM.App.Models;
using EM.App.OAuth.Managers;
using Microsoft.Owin.Security.Cookies;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using System.Web;
using Microsoft.Owin.Cors;
using System.Web.Http.Cors;
using System.Threading.Tasks;
using System.Web.Cors;
using System.Threading;

[assembly: OwinStartup(typeof(EM.App.OAuth.StartupDev))]

namespace EM.App.OAuth {
    public class StartupDev {
        private static OAuthBearerAuthenticationOptions oAuthBearerOptions;
        private static OAuthAuthorizationServerOptions oAuthAuthorizationServerOptions;
        public void Configuration(IAppBuilder app) {

            var config = new HttpConfiguration();

            config.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling =
                ReferenceLoopHandling.Serialize;
            config.Formatters.JsonFormatter.SerializerSettings.PreserveReferencesHandling =
                PreserveReferencesHandling.Objects;

            ConfigureOAuth(app);
            ConfigureAuth(app);

            config.MapHttpAttributeRoutes();

            var corsAttr = new EnableCorsAttribute("*", "*", "*");

            // Enable CORS for ASP.NET Identity
            app.UseCors(CorsOptions.AllowAll);
            //app.UseCors(new CorsOptions {
            //    PolicyProvider = new CorsPolicyProvider {
            //        PolicyResolver = request =>
            //            Task.FromResult(new CorsPolicy() {
            //                AllowAnyHeader = true, AllowAnyMethod = true,
            //                AllowAnyOrigin = true, PreflightMaxAge = 600,
            //            })
            //    }
            //});

            // Enable CORS for Web API
            config.EnableCors(corsAttr);
            app.UseWebApi(config);

        }

        private void ConfigureOAuth(IAppBuilder appBuilder) {
            oAuthAuthorizationServerOptions = oAuthAuthorizationServerOptions ?? (
                oAuthAuthorizationServerOptions = new OAuthAuthorizationServerOptions() {
                    TokenEndpointPath = new Microsoft.Owin.PathString("/token"),
                    AccessTokenExpireTimeSpan = TimeSpan.FromDays(1),
                    AllowInsecureHttp = true,
                    Provider = new SimpleAuthorizationServerProvider()
                });

            appBuilder.UseOAuthAuthorizationServer(oAuthAuthorizationServerOptions);
            appBuilder.UseOAuthBearerAuthentication(oAuthBearerOptions ?? (oAuthBearerOptions = new OAuthBearerAuthenticationOptions()));
        }
        public void ConfigureAuth(IAppBuilder app) {
            // Configure the db context, user manager and signin manager to use a single instance per request
            app.CreatePerOwinContext(ApiDbContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);
            app.CreatePerOwinContext<ApplicationSignInManager>(ApplicationSignInManager.Create);

            // Configure the sign in cookie
            app.UseCookieAuthentication(new CookieAuthenticationOptions {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
                Provider = new CookieAuthenticationProvider {
                    OnApplyRedirect = ctx => {
                        if (!IsAjaxRequest(ctx.Request)) {
                            //ctx.Response.Redirect(ctx.RedirectUri);
                        }
                    }
                }
            });
        }
        private static bool IsAjaxRequest(IOwinRequest request) {
            IReadableStringCollection query = request.Query;
            if ((query != null) && (query["X-Requested-With"] == "XMLHttpRequest")) {
                return true;
            }
            IHeaderDictionary headers = request.Headers;
            return ((headers != null) && (headers["X-Requested-With"] == "XMLHttpRequest"));
        }
    }
}