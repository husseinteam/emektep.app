﻿using Microsoft.Owin.Security.OAuth;
using System.Threading.Tasks;
using System.Security.Claims;
using EM.App.Models;
using System.Linq;
using Microsoft.AspNet.Identity;
using EM.App.OAuth.Managers;
using Microsoft.AspNet.Identity.EntityFramework;

namespace EM.App.OAuth.Providers {
    public class SimpleAuthorizationServerProvider : OAuthAuthorizationServerProvider {
        // OAuthAuthorizationServerProvider sınıfının client erişimine izin verebilmek için ilgili ValidateClientAuthentication metotunu override ediyoruz.
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context) {
            context.Validated();
        }

        // OAuthAuthorizationServerProvider sınıfının kaynak erişimine izin verebilmek için ilgili GrantResourceOwnerCredentials metotunu override ediyoruz.
        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context) {
            // CORS ayarlarını set ediyoruz.
            context.OwinContext.Response.Headers.Remove("Access-Control-Allow-Origin");
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });

            using (var db = new ApiDbContext()) {
                var dbuser = db.Users.SingleOrDefault(u => u.UserName == context.UserName);
                if (dbuser != null) {
                    var userManager = new ApplicationUserManager(
                        new UserStore<ApplicationUser>(db));

                    var pwdMatches = userManager.PasswordHasher
                        .VerifyHashedPassword(dbuser.PasswordHash, context.Password);
                    if (pwdMatches == PasswordVerificationResult.Success) {
                        var identity = new ClaimsIdentity(context.Options.AuthenticationType);
                        identity.AddClaim(new Claim("user_name", context.UserName));
                        //context.Scope.ToList().ForEach(scp => identity.AddClaim(new Claim(ClaimTypes.Role, scp)));
                        context.Validated(identity);
                    } else {
                        context.SetError("invalid_grant", $"Sayın {context.UserName}, Şifrenizi Yanlış Girdiniz");
                    }
                } else {
                    context.SetError("invalid_grant", $"{context.UserName} Adıyla Bir Kullanıcı Kayıtlı Değil");
                }
            }
        }
    }
}