﻿using System.ComponentModel.DataAnnotations;
using System.Net.Http;

namespace EM.App.ViewModels.Account {
    public class TokenResponseViewModel {

        public TokenResponseViewModel(string token, string type, string expires) {
            this.access_token = token;
            this.token_type = type;
            this.expires_in = expires;
        }

        public TokenResponseViewModel() {

        }   

        public string access_token { get; private set; }
        public string token_type { get; private set; }
        public string expires_in { get; private set; }

    }
}
