﻿using System.ComponentModel.DataAnnotations;

namespace EM.App.ViewModels.Account {
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
