﻿using System.ComponentModel.DataAnnotations;

namespace EM.App.ViewModels.Account {
    public class ForgotPasswordViewModel {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
