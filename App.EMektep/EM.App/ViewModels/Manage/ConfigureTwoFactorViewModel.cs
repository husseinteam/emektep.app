﻿using System.Collections.Generic;
using System.Web.WebPages.Html;

namespace EM.App.ViewModels.Manage {
    public class ConfigureTwoFactorViewModel {
        public string SelectedProvider { get; set; }

        public ICollection<SelectListItem> Providers { get; set; }
    }
}
