
using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using Microsoft.AspNet.Identity.EntityFramework;
using EM.App.Models;

namespace EM.App.Configurations {

    public class InitializerConfiguration : DbConfiguration {
        public InitializerConfiguration() {
            this.SetDatabaseInitializer<ApiDbContext>(new MigrateDatabaseToLatestVersion<ApiDbContext, MigrationConfiguration>());
        }
        
    }
}
