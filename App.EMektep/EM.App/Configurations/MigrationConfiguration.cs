
using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using Microsoft.AspNet.Identity.EntityFramework;
using EM.App.Models;
using EM.App.OAuth.Managers;
using EM.Extension.Core;

namespace EM.App.Configurations {
    internal sealed class MigrationConfiguration : DbMigrationsConfiguration<ApiDbContext> {
        public MigrationConfiguration() {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
            ContextKey = "Api";
        }

        protected override void Seed(ApiDbContext context) {
            var userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(context));
            var roleManager = new ApplicationRoleManager(new RoleStore<ApplicationRole>(context));

            var devRole = "developer";
            var role = roleManager.FindByNameAsync(devRole).Result;
            if (role == null) {
                role = new ApplicationRole(devRole);
                roleManager.CreateAsync(role).Wait();
            }

            new[] { "admin", "lecturer", "parent", "student" }.ToList().ForEach( rl => {
                var r = roleManager.FindByNameAsync(rl).Result;
                if (r == null) {
                    r = new ApplicationRole(rl);
                    roleManager.CreateAsync(r).Wait();
                }
            });

            if (context.Users.Any()) {
                context.Users.ToList().ForEach(u => context.Users.Remove(u));
                context.SaveChanges();
            }
            var emails = new[] { "huseyins@okul.com", "onders@okul.com" };
            var firstNames = new[] { "H�seyin", "�nder" };
            var lastNames = new[] { "S�nmez", "S�nmez" };

            for (var i = 0; i < 2; i++) {
                var email = emails.ElementAt(i);
                var up = new UserProfile() {
                    PhoneAlternate = "536033171{0}".Puts(i),
                    Position = "Chief Project Manager-{0}".Puts(i)
                };
                context.UserProfiles.Add(up);
                context.SaveChanges();
                var user = new ApplicationUser() {
                    FirstName = firstNames.ElementAt(i),
                    LastName = lastNames.ElementAt(i),
                    UserName = email,
                    Email = email,
                    EmailConfirmed = true,
                    UserType = EUserType.Admin,
                    UserProfile = up
                };
                userManager.CreateAsync(user, "P2ssw0rd!").Wait();
                if (!userManager.IsInRoleAsync(user.Id, devRole).Result) {
                    userManager.AddToRoleAsync(user.Id, devRole).Wait();
                }
            }
            context.SaveChanges();
        }
        
    }
}
