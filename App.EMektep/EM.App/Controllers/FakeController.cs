﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using EM.App.Models;
using EM.Extension.Core;

namespace EM.App.Controllers {
    [RoutePrefix("api/fake")]
    public class FakeController : ApiController {

        private static IEnumerable<TItem> GenerateFakeCollection<TItem>(int count, Func<int, TItem> singular) {
            for (int i = 0; i < count; i++) {
                yield return singular(i);
            }
        }

        [HttpGet]
        [Route("admins")]
        public async Task<IHttpActionResult> ListAdmin() {
            var resp = await Task.Run(() => GenerateFakeCollection<ApplicationUser>(20, (idx) =>
                new ApplicationUser() {
                    FirstName = "Özgür-{0}".Puts(idx),
                    LastName = "Sönmez-{0}".Puts(idx),
                    AccessFailedCount = 0,
                    Email = "lampiclobe-{0}@outlook.com".Puts(idx),
                    EmailConfirmed = true,
                    PasswordHash = "",
                    UserProfile = new UserProfile() {
                        PhoneAlternate = "0536033171-{0}".Puts(idx),
                        Position = "Kurumsal {0}. Yetkili".Puts(idx)
                    }
                } ));
            return Json(resp);
        }

    }
}
