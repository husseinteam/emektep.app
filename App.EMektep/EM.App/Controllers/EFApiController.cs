﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using EM.Extension.Core;
using EM.Repository;
using EM.Repository.Relayer;

namespace EM.App.Controllers {

    public class EFApiController<TContext, TEntity> : ApiController 
        where TEntity: class
        where TContext : DbContext {
        private Lazy<IGenericRepository<TEntity>> lazyRepo { get; } =
            new Lazy<IGenericRepository<TEntity>>(
                () => SRepository.GenerateEFRepository<TContext, TEntity>());

        internal IGenericRepository<TEntity> Repository {
            get {
                return lazyRepo.Value;
            }
        }

        internal async Task<IDMLResponse<TEntity>> GenericUpdate(TEntity model, params Func<TEntity, object>[] relationSelectors) {

            if (Request.GetRouteData().Values.Keys.Contains("id")) {
                var id = Request.GetRouteData().Values["id"].ParseTo<string>();
                return await Repository.Update(id, model, relationSelectors);
            } else {
                return null;
            }

        }

        internal async Task<IDMLResponse<TEntity>> GenericSingle(Expression<Func<TEntity, bool>> selector) {
            if (Request.GetRouteData().Values.Keys.Contains("id")) {
                var id = Request.GetRouteData().Values["id"].ParseTo<string>();
                var selected = await Repository.SelectBy(selector);
                return new DMLResponse<TEntity>(
                    await selected.CollectionResponse.AsQueryable().Where(selector).SingleOrDefaultAsync());
            } else {
                return null;
            }
        }

        internal async Task<IDMLResponse<TEntity>> GenericDelete() {

            if (Request.GetRouteData().Values.Keys.Contains("id")) {
                var id = Request.GetRouteData().Values["id"].ParseTo<string>();
                return await Repository.Delete(id);
            } else {
                return null;
            }

        }
    }
}