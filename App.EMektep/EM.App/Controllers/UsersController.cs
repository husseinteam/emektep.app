﻿using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Net.Http.Headers;
using System.Web.Http;
using EM.App.Models;
using EM.App.OAuth.Managers;
using EM.App.Services;
using EM.App.ViewModels.Account;
using Microsoft.AspNet.Identity.Owin;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using System.Net.Http;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using System;
using EM.App.ActionFilters;
using EM.Extension.Core;

namespace EM.App.Controllers {
    [RoutePrefix("api/users")]
    [Authorize]
    public class UsersController : EFApiController<ApiDbContext, ApplicationUser> {

        private readonly IEmailSender _emailSender;
        private readonly ISmsSender _smsSender;
        private readonly ApiDbContext _applicationDbContext;

        public UsersController() {
            _applicationDbContext = new ApiDbContext();
            _emailSender = new AuthMessageSender();
            _smsSender = new AuthMessageSender();
        }

        [Route("list/{utype}")]
        [HttpGet]
        public async Task<IHttpActionResult> List() {
            var utype = Request.GetRouteData().Values["utype"];
            if (utype != null) {
                var utypeEnum = utype.ToString().GenerateEnum<EUserType>();
                var resp = await base.Repository.SelectBy(u => u.UserType == utypeEnum);
                return Ok(resp);
            } else {
                return BadRequest($"Bad utype: {utype}");
            }
        }

        [Route("add")]
        [HttpPost]
        public async Task<IHttpActionResult> Add([FromBody] ApplicationUser user) {
            var resp = await base.Repository.Insert(user);
            return Ok(resp);
        }


        [Route("update")]
        [HttpPut]
        public async Task<IHttpActionResult> Update([FromBody] ApplicationUser user) {
            var resp = await base.Repository.Update(user.Id, user);
            return Ok(resp);
        }


        [Route("delete/{id}")]
        [HttpDelete]
        public async Task<IHttpActionResult> Delete() {
            var id = Request.GetRouteData().Values["id"];
            if (id != null) {
                var guid = Guid.Parse(id.ToString());
                var resp = await base.Repository.Delete(guid);
                return Ok(resp);
            } else {
                return BadRequest($"Bad id: {id}");
            }
        }

    }
}
