﻿using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Net.Http.Headers;
using System.Web.Http;
using EM.App.Models;
using EM.App.OAuth.Managers;
using EM.App.Services;
using EM.App.ViewModels.Account;
using Microsoft.AspNet.Identity.Owin;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using System.Net.Http;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using System;
using EM.App.ActionFilters;

namespace EM.App.Controllers {
    [RoutePrefix("api/account")]
    [Authorize(Roles = "developer")]
    public class AccountController : ApiController {
        private readonly ApplicationUserManager _userManager;
        private ApplicationSignInManager _signInManager {
            get {
                return new ApplicationSignInManager(_userManager, AuthenticationManager);
            }
        }
        private IAuthenticationManager AuthenticationManager {
            get {
                return Request.GetOwinContext().Authentication;
            }
        }
        private readonly IEmailSender _emailSender;
        private readonly ISmsSender _smsSender;
        private readonly ApiDbContext _applicationDbContext;

        public AccountController() {
            _applicationDbContext = new ApiDbContext();
            _userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(_applicationDbContext));
            _emailSender = new AuthMessageSender();
            _smsSender = new AuthMessageSender();
        }

        [Route("login")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<IHttpActionResult> Login([FromBody]LoginViewModel model, string returnUrl = null) {
            var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, false, false);
            if (result == SignInStatus.Success) {
                return Json(new {
                    message = "Login Succeeded",
                    done = true,
                    token_object = this.RequestToken(model)
                });
            } else {
                return Json(new {
                    message = "Invalid Login Attempt",
                    done = false,
                });
            }
        }

        [Route("logoff")]
        [HttpPost]
        public async Task<IHttpActionResult> LogOff() {
            try {
                await Task.Run(() => AuthenticationManager.SignOut());
                return Ok("Oturum Başarıyla Sonlandırldı");
            } catch (Exception ex) {
                return BadRequest(ex.Message);
            }
        }

        [Route("register")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<IHttpActionResult> Register([FromBody]RegisterViewModel model, string returnUrl = null) {
            var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
            var result = await _userManager.CreateAsync(user, model.Password);
            if (result.Succeeded) {
                // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=532713
                // Send an email with this link
                //var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                //var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Context.Request.Scheme);
                //await _emailSender.SendEmailAsync(model.Email, "Confirm your account",
                //    "Please confirm your account by clicking this link: <a href=\"" + callbackUrl + "\">link</a>");
                await _signInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                return Ok($"Hoşgeldiniz Sn. { user.UserName }");
            } else {
                return BadRequest($"{ user.Email } Kaydı Başarısız");
            }
        }

    }
}
