﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using Newtonsoft.Json;
using EM.App.Models;
using EM.App.Services;
using EM.Extension.Core;
using EM.Repository;

namespace EM.App.ActionFilters {

    public class ListUsersAsyncAttribute : ActionFilterAttribute {

        public override void OnActionExecuting(HttpActionContext actionContext) {
            // pre-processing
            //Debug.WriteLine("ACTION 1 DEBUG pre-processing logging");
        }
        public override async Task OnActionExecutedAsync(HttpActionExecutedContext actionExecutedContext, CancellationToken cancellationToken) {

            IDMLResponse<ApplicationUser> response = null;
            actionExecutedContext.Response.TryGetContentValue(out response);
            if (response != null) {
                (actionExecutedContext.ActionContext.Response.Content as ObjectContent).Value = 
                    response.CollectionResponse.Select(x => SResponseModifiers.UserModifier(x)).AsQueryable();
            }
            await base.OnActionExecutedAsync(actionExecutedContext, cancellationToken);
        }
        
    }

}