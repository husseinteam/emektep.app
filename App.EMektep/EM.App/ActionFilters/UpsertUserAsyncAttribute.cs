﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using Newtonsoft.Json;
using EM.App.Models;
using EM.App.Services;
using EM.Extension.Core;
using EM.Repository;

namespace EM.App.ActionFilters {

    public class UpsertUserAsyncAttribute : ActionFilterAttribute {

        private readonly string _parameterName;

        public UpsertUserAsyncAttribute(string parameterName) {
            _parameterName = parameterName;
        }
        
        public override async Task OnActionExecutingAsync(HttpActionContext actionContext, CancellationToken cancellationToken) {

            var model = actionContext.ActionArguments[_parameterName] as ApplicationUser;

            if (model != null) {
                using (var db = new ApiDbContext()) {
                    actionContext.ActionArguments[_parameterName] = new ApplicationUser {
                        UserName = model.Email,
                        Email = model.Email,
                        FirstName = model.FirstName,
                        LastName = model.LastName,
                        PhoneNumber = model.PhoneNumber,
                        UserProfile = db.UserProfiles.Find(model.UserProfile.ID)
                            .Set(x => x.Position = model.UserProfile.Position,
                                x => x.PhoneAlternate = model.UserProfile.PhoneAlternate) 
                            ?? new UserProfile() {
                                Position = model.UserProfile.Position,
                                PhoneAlternate = model.UserProfile.PhoneAlternate
                            }
                    }; 
                }
            }
            await base.OnActionExecutingAsync(actionContext, cancellationToken);
        }

        public override async Task OnActionExecutedAsync(HttpActionExecutedContext actionExecutedContext, CancellationToken cancellationToken) {

            IDMLResponse<ApplicationUser> response = null;
            actionExecutedContext.Response.TryGetContentValue(out response);
            if (response != null) {
                (actionExecutedContext.ActionContext.Response.Content as ObjectContent).Value =
                    new {
                        Data = SResponseModifiers.UserModifier(response.SingleResponse),
                        Fault = response.Fault,
                        Success = response.Success
                    };
            }
            await base.OnActionExecutedAsync(actionExecutedContext, cancellationToken);
        }

    }

}