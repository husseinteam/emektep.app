﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Filters;
using EM.App.Models;
using EM.App.Services;
using EM.Repository;

namespace EM.App.ActionFilters {

    public class SingleUserAsyncAttribute : ActionFilterAttribute {

        public override async Task OnActionExecutedAsync(HttpActionExecutedContext actionExecutedContext, CancellationToken cancellationToken) {

            IDMLResponse<ApplicationUser> response = null;
            actionExecutedContext.Response.TryGetContentValue(out response);
            if (response != null) {
                    var x = response.SingleResponse;
                (actionExecutedContext.ActionContext.Response.Content as ObjectContent).Value =
                    SResponseModifiers.UserModifier(x);
            }
            await base.OnActionExecutedAsync(actionExecutedContext, cancellationToken);
        }
    }

}