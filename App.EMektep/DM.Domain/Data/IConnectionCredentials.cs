﻿namespace DM.Domain.Data {

    public interface IConnectionCredentials {

        string ConnectionString();

    }

}
