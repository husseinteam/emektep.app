﻿using DM.Domain.Data;
using DM.Domain.Objects;
using Ninject;

namespace DM.Domain.Container {
    public static class SKernelHost {

        private static IKernel _Kernel;

        public static IKernel Kernel {
            get {
                if (_Kernel == null) {
                    _Kernel = new StandardKernel();
                    _Kernel.Bind(typeof(IDOTableBuilder<>)).To(typeof(DOTableBuilder<>));
                    _Kernel.Bind<IDataTools>().To<DataTools>();
                }
                return _Kernel;
            }
        }

    }
}
