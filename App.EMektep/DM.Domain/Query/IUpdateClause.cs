﻿using System;
using System.Linq.Expressions;
using DM.Domain.Objects;

namespace DM.Domain.Query {

    public interface IUpdateClause<TEntity>
        where TEntity : DOBase<TEntity> {

        IUpdateSetter<TEntity> Update<TProp>(Expression<Func<TEntity, TProp>> selector);
        void PersistUpdate(Action<int, TEntity> cursor);
    }

}