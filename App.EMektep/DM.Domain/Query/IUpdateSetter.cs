﻿
using DM.Domain.Objects;

namespace DM.Domain.Query {

    public interface IUpdateSetter<TEntity>
        where TEntity : DOBase<TEntity> {

        IUpdateClause<TEntity> Set<TProp>(TProp value);

    }

}