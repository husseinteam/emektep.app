﻿
using System;
using DM.Domain.Objects;

namespace DM.Domain.Query {

    public interface ICursorExecuter<TEntity>
         where TEntity : DOBase<TEntity> {

        void ExecuteOne(Action<IExecutedQuery<TEntity>> cursor, Action noneback = null);
        void ExecuteMany(Action<IExecutedQuery<TEntity>> cursor, Action noneback = null);

    }

}