﻿
using System;
using System.Collections.Generic;
using DM.Domain.Objects;

namespace DM.Domain.Query {

    public interface IQueryExecuter<TEntity>
         where TEntity : DOBase<TEntity> {

        void GenerateSingleExecutedQuery(Action<ExecutedQuery<TEntity>> doneback, 
            Action noneback = null);
        IEnumerable<IExecutedQuery<TEntity>> GenerateExecutedQueryList(Action noneback = null);

    }

}