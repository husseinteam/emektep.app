﻿using System;
using DM.Domain.Data;
using DM.Domain.Objects;

namespace DM.Domain.Query {

    public interface IExecutedQuery<TEntity>
        where TEntity : DOBase<TEntity> {

        int Index { get; }
        TEntity ResolvedEntity { get; }
        IUpdateClause<TEntity> Modify();
        IDataTools DataTools { get; }
        bool Delete(Action<Exception> fallback = null);

    }

}