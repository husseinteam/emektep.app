﻿
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using DM.Domain.Data;
using DM.Domain.Objects;

namespace DM.Domain.Query {

    public interface IQueryBuilder {

        IQueryBuilder AddField(PropertyInfo prop);
        IQueryBuilder RemoveField<TEntity>(PropertyInfo prop)
            where TEntity : DOBase<TEntity>;
        IQueryBuilder AndConstraint(PropertyInfo prop, object value, ExpressionType nodeType);
        IQueryBuilder OrConstraint(PropertyInfo prop, object value, ExpressionType nodeType);
        IQueryBuilder OrderByField(PropertyInfo prop);

        StringBuilder BuildQuery();
        List<PropertyInfo> Fields { get; }

        IDataTools DataTools { get; }

    }

}