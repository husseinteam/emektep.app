﻿using System;
using System.Linq.Expressions;
using DM.Domain.Objects;
using EM.Extension.Core;

namespace DM.Domain.Query {

    public class OrderedList<TEntity> : IOrderedList<TEntity> 
        where TEntity : DOBase<TEntity> {
        
        private SelectList<TEntity> _SelectList;

        public OrderedList(SelectList<TEntity> selectList) {
            this._SelectList = selectList;
        }
        
        public IOrderedList<TEntity> By<TProp>(Expression<Func<TEntity, TProp>> selector) {

            var param = selector.Parameters[0] as ParameterExpression;
            var prop = selector.ResolveMember();
            this._SelectList.QueryBuilder.OrderByField(prop);
            return this;

        }

        public void ExecuteOne(Action<IExecutedQuery<TEntity>> cursor, Action fallback = null) {
            this._SelectList.ExecuteOne(cursor, fallback);
        }

        public void ExecuteMany(Action<IExecutedQuery<TEntity>> cursor, Action noneback = null) {
            this._SelectList.ExecuteMany(cursor, noneback);
        }

    }

}