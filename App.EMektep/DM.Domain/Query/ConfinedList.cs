﻿using System;
using System.Linq.Expressions;
using DM.Domain.Objects;
using EM.Extension.Core;

namespace DM.Domain.Query {

    public class ConfinedList<TEntity> : IConfinedList<TEntity>
        where TEntity : DOBase<TEntity> {

        private SelectList<TEntity> _SelectList;

        public ConfinedList(SelectList<TEntity> selectList, Expression<Func<TEntity, bool>> confineClause =  null) {
            this._SelectList = selectList;
            if (confineClause != null) {
                this.And(confineClause);
            }
        }

        public IConfinedList<TEntity> And(Expression<Func<TEntity, bool>> confineClause) {

            BinaryExpression operation;
            System.Reflection.PropertyInfo prop;
            object value;
            GenerateConstraintParameters(confineClause, out operation, out prop, out value);
            this._SelectList.QueryBuilder.AndConstraint(prop, value, operation.NodeType);
            return this;

        }

        public IConfinedList<TEntity> Or(Expression<Func<TEntity, bool>> confineClause) {

            BinaryExpression operation;
            System.Reflection.PropertyInfo prop;
            object value;
            GenerateConstraintParameters(confineClause, out operation, out prop, out value);
            this._SelectList.QueryBuilder.OrConstraint(prop, value, operation.NodeType);
            return this;

        }
        
        public IOrderedList<TEntity> Order() {

            return new OrderedList<TEntity>(this._SelectList);

        }

        private static void GenerateConstraintParameters(Expression<Func<TEntity, bool>> confineClause, out BinaryExpression operation, out System.Reflection.PropertyInfo prop, out object value) {
            var param = confineClause.Parameters[0] as ParameterExpression;
            operation = confineClause.Body as BinaryExpression;
            prop = operation.Left.ExposeMember();
            var constant = operation.Right as ConstantExpression;
            if (constant == null) {
                value = operation.Right.Resultify();
            } else {
                value = constant.Value;
            }
        }

        public void ExecuteOne(Action<IExecutedQuery<TEntity>> cursor, Action noneback = null) {
            this._SelectList.ExecuteOne(cursor, noneback);
        }

        public void ExecuteMany(Action<IExecutedQuery<TEntity>> cursor, Action noneback = null) {
            this._SelectList.ExecuteMany(cursor, noneback);
        }

    }

}
