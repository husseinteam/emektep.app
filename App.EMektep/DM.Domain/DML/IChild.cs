﻿
using System;
using DM.Domain.Objects;

namespace DM.Domain.DML {

    public interface IChild<TEntity>
        where TEntity : DOBase<TEntity> {

        IChild<TOther> Single<TOther>(Func<IExecutedResult<TEntity>, TOther> cursor)
            where TOther : DOBase<TOther>;
        ISibling<TEntity> Sibling();

        void ExecuteMany(Action<IExecutedResultList> cursor, Action<Exception> fallback = null);

        void Upsert(Action<TEntity> callback, Action<Exception> fallback = null);

    }

}