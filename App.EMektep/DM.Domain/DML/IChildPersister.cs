﻿
using System;
using DM.Domain.Objects;

namespace DM.Domain.DML {

    public interface IChildPersister {

        IDOBase Persist(Action<Exception> fallback = null);

    }

}