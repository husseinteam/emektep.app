﻿
using System;
using System.Collections.Generic;
using DM.Domain.Objects;

namespace DM.Domain.DML {

    public interface IEntityDictionary {

        List<KeyValuePair<Type, object>> Dict { get; }

    }

}