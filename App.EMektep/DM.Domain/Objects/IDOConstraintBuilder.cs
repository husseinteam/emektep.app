﻿
using System;
using System.Linq.Expressions;

namespace DM.Domain.Objects {

    public interface IDOConstraintBuilder<TEntity> where TEntity : DOBase<TEntity> {

        DORelationBuilder ForeignKey<TKey>(params Expression<Func<TEntity, TKey>>[] selectors);

    }

}