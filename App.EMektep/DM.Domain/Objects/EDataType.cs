﻿
using System;
using System.Collections.Generic;
using DM.Domain.Data;

namespace DM.Domain.Objects {

    public enum EDataType {

        String,
        Int,
        Bool,
        Enum,
        DateTime,
        Date,
        UniqueIdentifier
    }

    public static class EDataTypeExtensions {

        public static string Parse(this EDataType self, EServerType sType) {
            switch (sType) {
                case EServerType.MySQL:
                    throw new NotImplementedException("MySql not supported");
                case EServerType.MSSql:
                    switch (self) {
                        case EDataType.String:
                            return "VARCHAR";
                        case EDataType.Int:
                        case EDataType.Enum:
                            return "INT";
                        case EDataType.Bool:
                            return "Bit";
                        case EDataType.DateTime:
                            return "DATETIME2(7)";
                        case EDataType.Date:
                            return "DATE";
                        case EDataType.UniqueIdentifier:
                            return "UNIQUEIDENTIFIER";
                        default:
                            throw new KeyNotFoundException("invalid EDataType");
                    }
                default:
                    throw new NotImplementedException("Unsupported option");
            }
        }

    }
}