﻿namespace DM.Domain.Objects {

    public interface ILineBuilder {

        string Build();

    }

}
