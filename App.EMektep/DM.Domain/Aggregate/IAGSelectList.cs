﻿using System;
using System.Linq.Expressions;
using DM.Domain.Objects;

namespace DM.Domain.Aggregate {

    public interface IAGSelectList<TEntity, T>
        where TEntity : DOBase<TEntity>
        where T : AGBase<T> {

        IAGSelectList<TEntity, T> Add<TProp>(Expression<Func<TEntity, TProp>> columnSelector, Expression<Func<T, TProp>> aliasSelector);
    }

}