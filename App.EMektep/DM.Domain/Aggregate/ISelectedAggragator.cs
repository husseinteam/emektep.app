﻿
using System;
using System.Linq.Expressions;

namespace DM.Domain.Aggregate {

    public interface ISelectedAggragator<T> : IConfinedAggragator<T>
        where T : AGBase<T> {

        IWheredAggragator<T> Where(Expression<Func<T, bool>> selector);

    }

}