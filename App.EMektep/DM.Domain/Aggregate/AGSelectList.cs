﻿
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using DM.Domain.Objects;
using EM.Extension.Core;

namespace DM.Domain.Aggregate {

    internal class AGSelectList<TEntity, T> : IAGSelectList<TEntity, T>
        where TEntity : DOBase<TEntity>
        where T : AGBase<T> {

        public AGSelectList() {
            _SelectedExpressions = new List<KeyValuePair<Expression, Expression>>();
        }

        private List<KeyValuePair<Expression, Expression>> _SelectedExpressions;

        public IAGSelectList<TEntity, T> Add<TProp>(Expression<Func<TEntity, TProp>> columnSelector, Expression<Func<T, TProp>> aliasSelector) {

            _SelectedExpressions.Add(new KeyValuePair<Expression, Expression>(
                aliasSelector.Body, columnSelector.Body));
            return this;

        }

        public override string ToString() {

            var str = new StringBuilder();
            foreach (var expkv in _SelectedExpressions) {
                var exp = expkv.Value;
                if (exp as MemberExpression != null) {
                    var prop = exp.ExposeMember();
                    var entity = Activator.CreateInstance(typeof(TEntity)) as IDOBase;
                    str.AppendFormat("{0}.[{1}] AS {2}, ", entity.SchemaBuilder.GetFormatted(), 
                        prop.Name, expkv.Key.ExposeMember().Name);
                } else {
                    var compositString = new StringBuilder();
                    var right = (exp as BinaryExpression).Right;
                    var entity = Activator.CreateInstance(typeof(TEntity)) as IDOBase;
                    Action<PropertyInfo> caseMember = (pr) => {
                        compositString.AppendFormat(" {0}.[{1}] ",
                            entity.SchemaBuilder.GetFormatted(), pr.Name);
                    };
                    Action<object> caseConstant = (value) => {
                        compositString.AppendFormat("+ '{0}' +", value);
                    };
                    var left = (exp as BinaryExpression).Left;
                    if (left is BinaryExpression) {
                        var left1 = (left as BinaryExpression).Left;
                        var left2 = (left as BinaryExpression).Right;
                        left1.ExtractComposit(caseMember, caseConstant);
                        left2.ExtractComposit(caseMember, caseConstant);
                    } else {
                        left.ExtractComposit(caseMember, caseConstant);
                    }
                    right.ExtractComposit(caseMember, caseConstant);
                    str.AppendFormat("{0} AS {1}, ", compositString, expkv.Key.ExposeMember().Name);
                }
            }
            return str.ToString().TrimEnd(' ', ',');
        }

    }

}