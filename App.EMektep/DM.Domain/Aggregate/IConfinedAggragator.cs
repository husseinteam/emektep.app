﻿
using System;

namespace DM.Domain.Aggregate {

    public interface IConfinedAggragator<T>
        where T : AGBase<T> {

        void ExecuteList(Action<T> cursor, Action lastback = null);
        void ExecuteSingle(Action<T> cursor);

    }

}