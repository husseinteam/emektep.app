﻿
using DM.Domain.Objects;

namespace DM.Domain.Aggregate {

    public interface IAGBase {

        IAGBase Drop();
        void BuildView();
        IAGSchemaBuilder GetSchemaBuilder();
        string Slug { get; }

    }

}