﻿using System;
using DM.Domain.Objects;
using EM.Extension.Core;

namespace DM.Domain.Extensions {

    internal static class EnumExtensions {

        internal static EDataType GetDataType(this Type self) {

            if (self.Equals(typeof(String))) {
                return EDataType.String;
            } else if (self.Equals(typeof(bool))) {
                return EDataType.Bool;
            } else if (self.Equals(typeof(DateTime))) {
                return EDataType.DateTime;
            } else if (self.IsEnum) {
                return EDataType.Enum;
            } else if (self.Equals(typeof(int?)) || self.IsNumericType()) {
                return EDataType.Int;
            } else {
                return EDataType.String;
            }

        }

    }

}
