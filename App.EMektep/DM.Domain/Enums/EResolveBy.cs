﻿namespace DM.Domain.Enums {
    public enum EResolveBy {
        AllAssigned,
        All,
        AllRequired,
        AllPKs
    }
}
