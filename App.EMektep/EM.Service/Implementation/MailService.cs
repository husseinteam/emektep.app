﻿using System;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.Mail;
using EM.Extension.Core;
using EM.Service.Contract;
using EM.Service.Enum;

namespace EM.Service.Implementation {

    class MailService : IMailService {
        
        public void SendMail(NameValueCollection values, MailAddressCollection to,
            EMailType mailType, Action callback, Action<Exception> fallback = null) {

            var credentials = new NetworkCredential(values["MailingUserName"], "SndGrd!293117");

            var myMessage = new MailMessage();
            myMessage.From = new MailAddress(values["MailingFromEmail"]);

            // Add multiple addresses to the To field.
            to.ToList().ForEach(add => myMessage.To.Add(add));

            myMessage.Subject = GetMailSubjectByType(mailType, values);

            //Add the HTML and Text bodies
            myMessage.Body = GetMailBodyByType(mailType, values);
            //myMessage.Text = "Hello World plain text!";

            //var apiClient = new SendGridAPIClient("");

            //try {
            //    //apiClient.client
            //} catch (Exception ex) {
            //    if (fallback != null) {
            //        fallback(ex);
            //    }
            //}

        }

        private static string GetMailBodyByType(EMailType mailType, NameValueCollection values) {

            switch (mailType) {
                case EMailType.ActivationMail:
                    return @"Please click this <a href=""{0}"">link</a>"
                        .Puts(values["ActivationLink"]);
                case EMailType.RecoverMail:
                    return @"Please click this <a href=""{0}"">link</a>"
                        .Puts(values["RecoverLink"]);
                default:
                    throw new NotSupportedException("Undefined EMailType");
            }

        }

        private static string GetMailSubjectByType(EMailType mailType, NameValueCollection values) {

            switch (mailType) {
                case EMailType.ActivationMail:
                    return "Aktivasyon E-Postası";
                case EMailType.RecoverMail:
                    return "Şifre Kurtarma E-Postası";
                default:
                    throw new NotSupportedException("Undefined EMailType");
            }

        }
    }

}
