﻿using DM.Domain.Objects;
using DM.Domain.Query;
using EM.Objects.DomainObjects.Soru;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EM.Test.UnitTests {

    [TestClass]
    public class QueryTests {

        [TestMethod]
        public void SelectRunsOK() {

            Q<Question>.SelectAllColumns().ExecuteMany((query) => {
                Assert.IsNotNull(query);
                Assert.IsNotNull(query.ResolvedEntity);
                Assert.AreEqual(EQuestionType.Test, query.ResolvedEntity.QuestionType);
            });

        }

        [TestMethod]
        public void SelectWithAddColumnsRunsOK() {

            Q<Question>.SelectAllColumns().Add(s => s.Title).Add(s => s.QuestionType).ExecuteMany((query) => {
                Assert.IsNotNull(query);
                Assert.IsNotNull(query.ResolvedEntity);
                Assert.AreEqual(EQuestionType.Test, query.ResolvedEntity.QuestionType);
                Assert.IsNull(query.ResolvedEntity.Title);
            });

        }

        [TestMethod]
        public void SelectWithRemoveColumnsRunsOK() {

            Q<Question>.SelectAllColumns().Remove(s => s.Title).ExecuteMany((query) => {
                Assert.IsNotNull(query);
                Assert.IsNotNull(query.ResolvedEntity);
                Assert.AreEqual(EQuestionType.Test, query.ResolvedEntity.QuestionType);
                Assert.IsNull(query.ResolvedEntity.Title);
            });

        }

        [TestMethod]
        public void SelectByOrderRunsOK() {

            Q<Question>.SelectAllColumns().Order().By(s => s.Title).ExecuteMany((query) => {
                Assert.IsNotNull(query);
                Assert.IsNotNull(query.ResolvedEntity);
                Assert.AreEqual(EQuestionType.Test, query.ResolvedEntity.QuestionType);
                Assert.IsNotNull(query.ResolvedEntity.Title);
            });

        }

        [TestMethod]
        public void SelectWhereRunsOK() {

            Q<Question>.SelectAllColumns().Where(s => s.ID == 1).ExecuteMany((query) => {
                Assert.IsNotNull(query);
                Assert.IsNotNull(query.ResolvedEntity);
                Assert.AreEqual(EQuestionType.Test, query.ResolvedEntity.QuestionType);
                Assert.AreEqual(1, query.ResolvedEntity.ID);
            });

        }

    }

}
