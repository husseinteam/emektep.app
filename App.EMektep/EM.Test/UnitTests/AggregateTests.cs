﻿using DM.Domain.Aggregate;
using EM.Entities.AggragateObjects.Tedrisat;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EM.Test.UnitTests {

    [TestClass]
    public class AggregateTests {

        [TestMethod]
        public void DersListesiAGAllOK() {

            AG<DersListesiAG>.Views().SelectAllColumns().ExecuteList(dersl => {
                Assert.IsNotNull(dersl);
                Assert.IsNotNull(dersl.OgretimUyesi);
            });

        }

        [TestMethod]
        public void DersListesiAGDistinctlOK() {

            var c = 0;
            AG<DersListesiAG>.Views().SetDistinctColumn(d => d.UyeID)
                .SelectAllColumns()
                .Where(ou => ou.UyeID == 1)
                .ExecuteList(dersl => {
                    Assert.IsNotNull(dersl);
                    Assert.IsNotNull(dersl.OgretimUyesi);
                    c++;
                });
            Assert.AreEqual(22, c);

        }

        [TestMethod]
        public void DersListesiAGDersSaatiTeorikSaatIs3OK() {

            var c = 0;
            AG<DersListesiAG>.Views().SelectAllColumns().Where(dl => dl.TeorikSaat == 3).ExecuteList(dersl => {
                Assert.IsNotNull(dersl);
                Assert.IsNotNull(dersl.OgretimUyesi);
                c++;
            });
            Assert.AreNotEqual(0, c);
        }

        [TestMethod]
        public void DersListesiAGUygulamaSaatiNot0OK() {

            var c = 0;
            AG<DersListesiAG>.Views().SelectAllColumns().Where(dl => dl.UygulamaSaat != 0).ExecuteList(dersl => {
                Assert.IsNotNull(dersl);
                Assert.IsNotNull(dersl.OgretimUyesi);
                c++;
            });
            Assert.AreNotEqual(0, c);
        }

        [TestMethod]
        public void DersListesiAGTeorikSaatIs3AndAlsoUygulamaSaatIs0OK() {

            var c = 0;
            AG<DersListesiAG>.Views().SelectAllColumns()
                .Where(dl => dl.TeorikSaat == 3)
                .AndAlso(dl => dl.UygulamaSaat == 0)
                .ExecuteList(dersl => {
                    Assert.IsNotNull(dersl);
                    Assert.IsNotNull(dersl.OgretimUyesi);
                    c++;
                });
            Assert.AreNotEqual(0, c);
        }

    }

}
